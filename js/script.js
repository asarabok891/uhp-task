function initializeStickyMenu()
{   
    var header = $('header');
    header.sticky({
        topSpacing:0,
        zIndex: 9999
    });
    var nav = $('.nav');
    $(window).on('scroll resize load', function(e) {
        if(nav.hasClass('visible'))
        {
            nav.removeClass('visible');
        }
        if(window.innerWidth >= 1360)
        {
            if( window.pageYOffset == 0 )
            {
                header.removeClass('header__sticky');
                toggleLogoClass('logo-img-blue');
            }
            else
            {
                header.addClass('header__sticky');
                toggleLogoClass('logo-img-white');
            }
            header.on('sticky-start', function() 
            { 
                $(this).addClass('header__sticky');
                toggleLogoClass('logo-img-white');
            });
        
            header.on('sticky-end', function() 
            { 
                $(this).removeClass('header__sticky');
                toggleLogoClass('logo-img-blue');
            });
        }
        else
        {
            hideLogos();
            toggleLogoClass('logo-img-white');
        }
    });
}

function toggleLogoClass(logo_to_show)
{
    hideLogos();
    $('.'+logo_to_show).show();
}

function hideLogos()
{
    var logo_blue = $('.logo-img-blue');
    var logo_white = $('.logo-img-white'); 
    logo_blue.hide();
    logo_white.hide();
}

function initializeSwiperSlider()
{
    $(window).on('scroll resize load', function(e) {
        if(window.innerWidth >= 960)
        {
            var swiper = new Swiper('.swiper-container', {
                freeMode: true,
                slidesPerView: 4,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });
        }
        else
        {
            var swiper = new Swiper('.swiper-container', {
                freeMode: true,
                slidesPerView: 2,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });
        }
    });
}

var counter_finished = false;
function initializeAnimatedCounter()
{
    $(window).on('scroll resize load', function(e) {
        if( !counter_finished && $('#facts-services').visible(true) )
        {
            var options = 
            {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                suffix: '%'
            };
            var web_development = new CountUp('service-web', 0, 85, 0, 2.5, options);
            var mobile_development = new CountUp('service-mobile', 0, 90, 0, 2.5, options);
            var voice = new CountUp('service-voice', 0, 69, 0, 2.5, options);
            var iot = new CountUp('service-iot', 0, 95, 0, 2.5, options);
    
            web_development.start();
            mobile_development.start();
            voice.start();
            iot.start();
    
            counter_finished = true;
        }
    });
}

function initializeScrollTipListener()
{
    $('.footer--gototop').click(function(e){
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });
}

function initializeAnimations()
{
    var animation_fade_in_up="animated fadeInUp slow";
    var animation_fade_in_left="animated fadeInLeft slow";
    var animation_fade_in_right="animated fadeInRight slow";
    
    var main_title = $('.main--title');
    var main_action_button = $('.main--action-button');
    var service_box = $('.service-box');
    var herbalife_image = $('.herbalife-image');
    var title_strong = $('.facts--title__strong');
    var title_light = $('.facts--title__light');
    var news = $('.news--list');

    $(window).on('scroll resize load', function(e) {
        if( !main_title.hasClass('animated') && main_title.visible(true) )
        {
            main_title.addClass(animation_fade_in_left);
        }
        if( !main_action_button.hasClass('animated') && main_action_button.visible(true) )
        {
            main_action_button.addClass(animation_fade_in_up);
        }
        if( !service_box.hasClass('animated') && service_box.visible(true) )
        {
            service_box.addClass(animation_fade_in_up);
        }
        if( !herbalife_image.hasClass('animated') && herbalife_image.visible(true) )
        {
            herbalife_image.addClass(animation_fade_in_up);
        }
        if( !title_strong.hasClass('animated') && title_strong.visible(true) )
        {
            title_strong.addClass(animation_fade_in_left);
        }
        if( !title_light.hasClass('animated') && title_light.visible(true) )
        {
            title_light.addClass(animation_fade_in_right);
        }
        if( !news.hasClass('animated') && news.visible(true) )
        {
            news.addClass(animation_fade_in_up);
        }
    });
}

function initializeToggleMobileNavMenu()
{
    var menu = $('.header--mobile-menu');
    var nav = $('.nav');

    menu.click(function(){
        if( nav.is(':visible') )
        {
            nav.removeClass('visible');
        }
        else
        {
            nav.addClass('visible');
        }
    });
}


$(document).ready(function()
{
    initializeToggleMobileNavMenu();
    initializeStickyMenu();
    initializeSwiperSlider();
    initializeAnimatedCounter();
    initializeScrollTipListener();
    initializeAnimations();
});